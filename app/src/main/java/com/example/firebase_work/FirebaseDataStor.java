package com.example.firebase_work;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseDataStor extends AppCompatActivity {
EditText editText,editText1;
Button button,loadbutton;
DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_data_stor);
        editText=findViewById(R.id.nameid);
        editText1=findViewById(R.id.ageid);
        button=findViewById(R.id.buttonid);
        loadbutton=findViewById(R.id.Loadbuttonid);

        databaseReference= FirebaseDatabase.getInstance().getReference("Student");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });

        loadbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(FirebaseDataStor.this,LoadDataActivity.class);
                startActivity(intent);
            }
        });

    }

    private void saveData() {
        String name=editText.getText().toString().trim();
        String password=editText1.getText().toString().trim();

        String key=databaseReference.push().getKey();

        student s=new student(name,password);
        databaseReference.child(key).setValue(s);
        Toast.makeText(getApplicationContext(),"Student infu is Success",Toast.LENGTH_LONG).show();

        editText.setText("");
        editText1.setText("");
    }

}
