package com.example.firebase_work;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Sign_in extends AppCompatActivity implements View.OnClickListener {
    private EditText editText,editText1;
    private TextView textView;
    private Button button;
    ProgressBar progressBar;
    private FirebaseAuth firebaseAuth;

    //Remember Password
    private CheckBox checkBox;
    private SharedPreferences sharedPreferences;
    private static final String PREES_NAME="PrefsFile";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        this.setTitle("Sign in");

        firebaseAuth = FirebaseAuth.getInstance();
        editText=(EditText)findViewById(R.id.SinName);
        editText1=(EditText)findViewById(R.id.SinPass);
        textView=(TextView)findViewById(R.id.SinText);
        button=(Button)findViewById(R.id.SignIn);
        progressBar=(ProgressBar)findViewById(R.id.SinPro);

        //Remember Work
        checkBox=(CheckBox)findViewById(R.id.checkRemmber);
        sharedPreferences=getSharedPreferences(PREES_NAME,MODE_PRIVATE);
        checkBox.setOnClickListener(this);
        getPerferencesData();

        textView.setOnClickListener(this);
        button.setOnClickListener(this);


    }
//Remember WOrk
    private void getPerferencesData() {
        SharedPreferences sp=getSharedPreferences(PREES_NAME,MODE_PRIVATE);
        if (sp.contains("email"))
        {
            String e=sp.getString("email","not found");
            editText.setText(e.toString());
        }
        if (sp.contains("password"))
        {
            String p=sp.getString("password","not found");
            editText1.setText(p.toString());
        }
        if (sp.contains("check"))
        {
            boolean b=sp.getBoolean("check",false);
            checkBox.setChecked(b);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.SignIn:
                userLogin();
                break;

            case R.id.SinText:
                Intent a=new Intent(getApplicationContext(),Sign_up.class);
                startActivity(a);
                break;


        }
    }

    private void userLogin() {

        String email=editText.getText().toString().trim();
        String password=editText1.getText().toString().trim();


        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

            editText.setError("Email Not Valid");
            editText.requestFocus();
            return;
        }

        if (password.isEmpty()){

            editText1.setError("Password Null");
            editText1.requestFocus();
            return;
        }
        if (password.length()<6){
            editText1.setError("Minimum Password 6 ");
            editText1.requestFocus();
            return;

        }
        progressBar.setVisibility(View.VISIBLE);

        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);
              if (task.isSuccessful())
                  {
                      //Remember Work
                      if (checkBox.isChecked())
                      {
                          boolean boolIsChecked=checkBox.isChecked();
                          SharedPreferences.Editor editor=sharedPreferences.edit();
                          editor.putString("email",editText.getText().toString());
                          editor.putString("password",editText1.getText().toString());
                          editor.putBoolean("check",boolIsChecked);
                          editor.apply();
                          Toast.makeText(getApplicationContext(),"Password have been Saved",Toast.LENGTH_LONG).show();

                      }else
                      {
                          sharedPreferences.edit().clear().apply();
                      }



                     Intent intent=new Intent(getApplicationContext(),ImageStorageFirebase.class);
                     intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                     startActivity(intent);
                  }
                  else
                      { Toast.makeText(getApplication(),"Login Unsuccessful",Toast.LENGTH_LONG).show();
                      }
            }
        });
    }
}
