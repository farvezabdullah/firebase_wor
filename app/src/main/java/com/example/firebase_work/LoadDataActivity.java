package com.example.firebase_work;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class LoadDataActivity extends AppCompatActivity {
private ListView listView;
DatabaseReference databaseReference;
private List<student>studentList;
private CustomAdaptar customAdaptar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_data);
        listView=findViewById(R.id.listid);
        databaseReference= FirebaseDatabase.getInstance().getReference("Student");
        studentList=new ArrayList <>();
        customAdaptar=new CustomAdaptar(LoadDataActivity.this,studentList);

    }

    @Override
    protected void onStart() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                studentList.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    student student=dataSnapshot1.getValue(student.class);
                    studentList.add(student);

                }
                listView.setAdapter(customAdaptar);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        super.onStart();
    }
}
