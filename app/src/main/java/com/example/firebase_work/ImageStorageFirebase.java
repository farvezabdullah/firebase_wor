package com.example.firebase_work;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.renderscript.Script;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class ImageStorageFirebase extends AppCompatActivity implements View.OnClickListener {
private Button chooseButton,saveButton,displayButton;
private EditText imageNameButton;
private ImageView imageView;
private ProgressBar progressBar;
private Uri uri;

    //menu work
FirebaseAuth auth;

DatabaseReference databaseReference;
    private StorageReference mStorageRef;

StorageTask storageTask;

private static final int IMAGE_REQUEST=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_storage_firebase);
        chooseButton=findViewById(R.id.chooseId);
        saveButton=findViewById(R.id.saveImageId);
        displayButton=findViewById(R.id.displayImageId);
        imageView=findViewById(R.id.imageId);
        progressBar=findViewById(R.id.progressid);
        imageNameButton=findViewById(R.id.imageNameId);

        //menu work
        auth=FirebaseAuth.getInstance();

        databaseReference= FirebaseDatabase.getInstance().getReference("Upload");
        mStorageRef = FirebaseStorage.getInstance().getReference("Upload");

        saveButton.setOnClickListener(this);
        chooseButton.setOnClickListener(this);
        displayButton.setOnClickListener(this);

    }

    @Override
    //menu work
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_layout,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==R.id.SignOutMenuId){
            FirebaseAuth.getInstance().signOut();
            finish();
            Intent intent=new Intent(getApplicationContext(),Sign_in.class);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.dataStorageMenuId){
            Intent intent=new Intent(getApplicationContext(),FirebaseDataStor.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }





    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.saveImageId:
               if (storageTask!=null && storageTask.isInProgress())
                {
                    Toast.makeText(getApplication(),"Uploading is progress",Toast.LENGTH_LONG).show();
                }else
                    {
                      saveData();
                    }

                break;

            case R.id.chooseId:
                    openFileChosser();
                break;

            case R.id.displayImageId:
Intent intent=new Intent(ImageStorageFirebase.this,DisplayImage.class);
startActivity(intent);
                break;

        }
    }

    void openFileChosser()
    {
        Intent intent=new Intent();
        intent.setType("image/*");
        intent.setAction(intent.ACTION_GET_CONTENT);
       startActivityForResult(intent,IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==IMAGE_REQUEST && resultCode==RESULT_OK && data!=null && data.getData()!=null)
        {
            uri=data.getData();
            Picasso.with(this).load(uri).into(imageView);
        }
    }

    public String getFileExtension(Uri uri)
    {
        ContentResolver contentResolver=getContentResolver();
        MimeTypeMap mimeTypeMap=MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void saveData()
       {
          final String imageName=imageNameButton.getText().toString().trim();
          if (imageName.isEmpty())
          {
             imageNameButton.setError("Enter your image name");
              imageNameButton.requestFocus();
              return;
           }

           StorageReference ref=mStorageRef.child(System.currentTimeMillis()+"."+getFileExtension(uri));

           ref.putFile (uri)
                   .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                       @Override
                       public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                           // Get a URL to the uploaded content
                           Toast.makeText(getApplication(),"Image is storage Successfull",Toast.LENGTH_LONG).show();

                           Task<Uri>urlTask=taskSnapshot.getStorage().getDownloadUrl();
                           while (!urlTask.isSuccessful());
                           Uri downloadUrl=urlTask.getResult();

                           Upload upload=new Upload(imageName,downloadUrl.toString());
                           String uploadId =databaseReference.push().getKey();
                           databaseReference.child(uploadId).setValue(upload);
                       }
                   })
                   .addOnFailureListener(new OnFailureListener() {
                       @Override
                       public void onFailure(@NonNull Exception exception) {
                           // Handle unsuccessful uploads
                           // ...
                           Toast.makeText(getApplication(),"Image is not storage Successfull",Toast.LENGTH_LONG).show();
                       }
                   });

    }
}
