package com.example.firebase_work;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CustomAdaptar extends ArrayAdapter<student> {
    private Activity context;
    private List<student> studentList;

    public CustomAdaptar(Activity context, List<student> studentList) {
        super(context,R.layout.sample_layout,studentList);
        this.context = context;
        this.studentList = studentList;
    }


    @NonNull
    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        View view=layoutInflater.inflate(R.layout.sample_layout,null,true);

        student student=studentList.get(position);

        TextView textView=view.findViewById(R.id.sampleNameId);
        TextView textView1=view.findViewById(R.id.samplePassId);

        textView.setText("Name : "+student.getName());
        textView1.setText("Password : "+student.getPassword());

        return view;
    }
}
